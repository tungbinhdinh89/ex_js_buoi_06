// Bài 01: Tìm số chẵn và số lẽ nhỏ hơn 100
function timSoChanLe() {
  var inSoChan = document.getElementById('result');
  var inSoLe = document.getElementById('result1');
  var stringChan = '';
  var stringLe = '';
  for (var i = 0; i < 100; i++) {
    if (i % 2 === 0) {
      stringChan = stringChan + i + '   ';
      inSoChan.innerHTML = `Đây là số chẵn: ${stringChan}`;
    } else {
      stringLe = stringLe + i + '     ';

      inSoLe.innerHTML = `Đây là số lẽ: ${stringLe}`;
    }
  }
}

// Bài 02: Đếm số chia hết cho 3
function soChiaHetCho3() {
  var count = 0;
  for (var i = 0; i <= 1000; i++) {
    if (i % 3 === 0) {
      count++;
      document.getElementById(
        'result2'
      ).innerHTML = `Số chia hết cho 3 nhỏ hơn 1.000 có : ${count} số`;
    }
  }
}
// Bài 03: Tìm số nguyên dương nhỏ nhất
var n = 0;
var s = 0;
// function soNguyenDuongNhoNhat() {}
// sử dụng vòng lặp while với điều kiện S < 10000
function soNguyenDuongNhoNhat() {
  while (s < 10000) {
    //bước nhảy của vòng lặp là n++
    n++;
    //cứ sau mỗi vòng lặp thì tổng S sẽ được cộng dồn với n cho đến khi S > 10000 thì thoát khỏi vòng lặp
    s = s + n;
  }

  //sau khi thoát thì ta có n và tổng S
  document.getElementById(
    'result3'
  ).innerHTML = `Số nguyên dương nhỏ nhất : ${n} `;
}

// Bài 04: Tính tổng
function tinhTong() {
  var n = document.getElementById('nhap_so_n').value * 1;
  var x = document.getElementById('nhap_so_x').value * 1;
  var luyThua = 1;
  var s = 0;
  for (i = 1; i <= n; i++) {
    luyThua = luyThua * x;
    console.log('luyThua: ', luyThua);
    s += luyThua;
  }
  document.getElementById('result4').innerHTML = `Tổng : ${s}`;
}
// Bài 05: Tính giai thừa
function tinhGiaiThua() {
  var soN = document.getElementById('so_n').value * 1;
  var giaiThua = 1;
  for (i = 1; i <= soN; i++) {
    giaiThua *= i;
  }
  document.getElementById('result5').innerHTML = `Giai Thừa : ${giaiThua}`;
}
// Bài 06: Tạo thẻ DIV
function taoThe() {
  var divs = document.querySelectorAll('.bai06');
  console.log('divs: ', divs);
  for (i = 0; i <= divs.length; i++) {
    if (i % 2 === 0) {
      divs[i].style.background = 'red';
      divs[i].innerHTML = `<p style='color: white'>Đây là số chẵn</p>`;
      console.log('i: ', i);
    } else {
      divs[i].style.background = 'blue';
      console.log('i le: ', i);
      divs[i].innerHTML = `<p style='color: white'>Đây là số lẽ</p>`;
    }
  }
}
